# Genshin Impact Music Downloader

Scripts for downloading Genshin Impact Music.

## Download Available Audio from The Wikia

Scrape the wikia HTML for audio files and then download them all.

```bash
curl https://genshin-impact.fandom.com/wiki/Soundtrack | grep -o 'https://static.wikia.nocookie.net/gensin-impact/.*\.ogg' | parallel wget {}
```

After downloading, they can be sorted into folders. e.g.

```bash
mkdir 'City of Winds and Idylls'
mv Soundtrack_City_of_Winds_and_Idylls_*.ogg 'City of Winds and Idylls'

cd 'City of Winds and Idylls'
for a in Soundtrack_*.ogg; do mv $a ${a:11}; done
cd -
```

## Download from YouTube and Split Into Tracks

The script `split_video.py` parses timestamp information and invokes `ffmpeg`
to produce individual songs from a YouTube video of an album.

1. use `youtube-dl` to download the video
2. copy-and-paste the timestamps from the Video description into a plaintext file
3. run `python split_video.py`, e.g.

```bash
mkdir 'The Stellar Moments'
cd 'The Stellar Moments'
youtube-dl 'https://invidious.048596.xyz/watch?v=zmS7kzSHuSE'
python split_video.py 'Genshin Impact Character OST Album - The Stellar Moments-zmS7kzSHuSE.mkv' < the_stellar_moments_timestamps.txt
rm 'Genshin Impact Character OST Album - The Stellar Moments-zmS7kzSHuSE.mkv'
```
