import sys
import csv
import subprocess as sp
from concurrent.futures import ThreadPoolExecutor


def get_media_duration(filename: str) -> str:
    return sp.check_output(
        [
            'ffprobe', '-show_entries', 'format=duration', '-of', 'default=noprint_wrappers=1:nokey=1',
            filename
        ],
        text=True
    ).rstrip()


if not len(sys.argv) == 2:
    print(f'usage: python {sys.argv[0]} downloaded_video.mp4 < timestamps.txt')
    sys.exit(1)

src_filename = sys.argv[1]

names = []
times = []
for line in csv.reader(sys.stdin, delimiter='-'):
    names.append(line[0][5:].rstrip())
    times.append(line[1].lstrip())

times.append(get_media_duration(src_filename))

jobs = [
    [
        'ffmpeg', '-i', src_filename, '-ss', times[i], '-to', times[i + 1],
        f'{name}.ogg'
    ]
    for i, name in enumerate(names)
]

with ThreadPoolExecutor(max_workers=4) as pool:
    pool.map(sp.run, jobs)
